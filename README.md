## Note: This project doesn't get updated anymore, since Shortwave does use its own radio-browser.info API wrapper now.

https://gitlab.gnome.org/World/Shortwave/tree/master/src/api

# rustio
A radio-browser.info API wrapper for rust. 

- [Open project homepage](https://gitlab.gnome.org/haecker-felix/Rustio)
- [Open documentation](https://docs.rs/rustio/)
- [rustio on crates.io](https://crates.io/crates/rustio)
- [Example usage in Shortwave](https://gitlab.gnome.org/World/Shortwave)

## Implemented features
- [x] Server communication (JSON deserialize)
- [x] Basic elements (station, country, language, state, tag, codec)
- [x] Server stats
- [x] Error handling
- [ ] Editable stations
- [ ] Create new stations
- [x] Advanced station search
